import {validateNoteTitle} from "./modules/validation";
import {toggleForm} from "./toggle";

const noteButtonElement = document.getElementById("note-button");
const showButtonElement = document.getElementById("zobrazit-poznamky");
const addFormElement = document.getElementById("MyForm");
const updateButtonElement = document.getElementById("update-button");



class NoteContainer {
    constructor() {
        noteButtonElement.addEventListener("click",toggleForm)
        this.notes = [];
    }

    addNote(note) {
        this.notes.push(note);
    }

    displayNotes() {
        const notesContainer = document.getElementById('notes-container');

        // Vymazání stávajících poznámek
        notesContainer.innerHTML = '';

        // Zkontrolujte, zda jsou poznámky dostupné
        if (this.notes.length === 0) {
            const noNotesMessage = document.createElement('p');
            noNotesMessage.textContent = 'Prozatím zde nejsou žádné poznámky.';
            notesContainer.appendChild(noNotesMessage);
            return;
        }
        this.notes.reverse();
        // Vytvoření HTML pro každou poznámku
        this.notes.forEach(function(note, index) {
            const noteElement = document.createElement('div');
            noteElement.className = 'note';

            const noteContent = document.createElement('div');
            noteContent.className = 'note-content';

            const titleElement = document.createElement('h2');
            titleElement.textContent = note.title;

            const contentElement = document.createElement('p');
            contentElement.textContent = note.content;

            const freeElement = document.createElement('p');
            freeElement.textContent = " ";

            const createdElement = document.createElement('p');
            createdElement.textContent = 'Vytvořeno: ' + (note.created instanceof Date ? note.created.toLocaleString() : 'Neznámé datum');

            const locationElement = document.createElement('p');
            locationElement.textContent = 'Geolokace: ' + note.location;

            const editButton = document.createElement('button');
            editButton.textContent = 'Upravit';
            editButton.className = 'edit-button';
            editButton.addEventListener('click', function() {
                // Volání funkce pro upravení poznámky
                editNoteContent(index);
            });

            const removeButton = document.createElement('button');
            removeButton.textContent = 'Odstranit';
            removeButton.className = 'delete-button'
            removeButton.addEventListener('click', function() {
                // Volání funkce pro odstranění poznámky
                removeNote(index);
            });

            noteElement.appendChild(titleElement);
            noteElement.appendChild(contentElement);
            noteElement.appendChild(freeElement);
            noteElement.appendChild(createdElement);
            noteElement.appendChild(locationElement);
            noteElement.appendChild(editButton);
            noteElement.appendChild(removeButton);

            notesContainer.appendChild(noteElement);
        });
    }
}

class UserNoteContainer extends NoteContainer {
    constructor(username) {
        super();
        showButtonElement.addEventListener("click", displayUserNotes);
        addFormElement.addEventListener("submit",addNote);
        this.username = username;
    }

    addNote(note) {
        note.user = this.username;
        super.addNote(note);
    }

    displayUserNotes() {
        const user = document.getElementById('username').value;

        if (user === '') {
            alert("Uživatel nesmí být prázdný!!!");
            return;
        }

        this.notes = JSON.parse(localStorage.getItem(user)) || [];
        this.notes.forEach(function(note) {
            if (typeof note.created === 'string') {
                note.created = new Date(note.created);
            }
        });
        this.displayNotes();
        this.displayUserInfo(user);
    }

    displayUserInfo(user) {
        const userInfoContainer = document.getElementById('user-info');
        userInfoContainer.textContent = 'Přihlášený uživatel: ' + user;
        userInfoContainer.style.display = 'block';
    }
}

const userNoteContainer = new UserNoteContainer('');

function displayUserNotes() {
    const user = document.getElementById('username').value;
    userNoteContainer.displayUserNotes(user);
}

export function addNote() {
    const successSound = new Audio('Sounds/addNoteSound.mp3');
    const titleInput = document.getElementById('title');
    const contentInput = document.getElementById('content');

    const title = titleInput.value;
    const content = contentInput.value;

    if (!validateNoteTitle(title)) {
        event.preventDefault();
        alert('Nadpis nesmí obsahovat čísla.');
        return;
    }

    // Kontrola vyplnění názvu a obsahu poznámky
    if (title.trim() === '' || content.trim() === '') {
        alert('Prosím vyplňte název a obsah poznámky.');
        return;
    }

    event.preventDefault();

    const user = document.getElementById('username').value;

    // Získání existujících poznámek uživatele z Local Storage
    const userNotes = JSON.parse(localStorage.getItem(user)) || [];

    // Vytvoření nové poznámky
    getLocation(function(location) {
        const note = {
            title: title,
            content: content,
            created: new Date(),
            location: location,
        };

        // Přidání nové poznámky do seznamu poznámek uživatele
        userNotes.push(note);

        // Uložení aktualizovaného seznamu poznámek uživatele do Local Storage
        localStorage.setItem(user, JSON.stringify(userNotes));

        // Resetování formuláře
        titleInput.value = '';
        contentInput.value = '';
        successSound.play();

        // Zobrazení poznámek uživatele
        userNoteContainer.displayUserNotes(user);
    });
}

function editNoteContent(index) {
    const note = userNoteContainer.notes[index];
    updateButtonElement.style.display = "block";

    // Zobrazení formuláře pro úpravu obsahu poznámky
    const noteForm = document.getElementById('note-form');
    const titleInput = document.getElementById('title');
    const contentInput = document.getElementById('content');
    const updateButton = document.getElementById('update-button');
    const addButton = document.getElementById('add-button');
    const successSound = new Audio('Sounds/GodDamn!.mp3');

    addButton.disabled = true;
    addButton.style.display = "none";
    titleInput.value = note.title;
    contentInput.value = note.content;

    // Nastavení funkce pro aktualizaci poznámky při kliknutí na tlačítko "Uložit"
    updateButton.onclick = function() {

        if (!validateNoteTitle(titleInput.value)) {
            event.preventDefault();
            alert('Nadpis nesmí obsahovat čísla.');
            return;
        }

        // Upravení obsahu poznámky
        note.title = titleInput.value;
        note.content = contentInput.value;

        // Aktualizace seznamu poznámek v Local Storage
        const user = document.getElementById('username').value;
        localStorage.setItem(user, JSON.stringify(userNoteContainer.notes));

        // Skrytí formuláře a aktualizace zobrazení poznámek
        noteForm.classList.remove('expanded');
        displayUserNotes();
        addButton.disabled = false;
        successSound.play();
        addButton.style.display = "block";
        titleInput.value = '';
        contentInput.value = '';
    };

    // Zobrazení formuláře pro úpravu obsahu poznámky
    noteForm.classList.add('expanded');
}

function removeNote(index) {
    // Odstranění poznámky ze seznamu
    userNoteContainer.notes.splice(index, 1);
    const deleteSound = new Audio('Sounds/delete.mp3');
    // Aktualizace seznamu poznámek v Local Storage
    const user = document.getElementById('username').value;
    localStorage.setItem(user, JSON.stringify(userNoteContainer.notes));

    // Zobrazení aktualizovaných poznámek
    deleteSound.play();
    displayUserNotes();
}

function getLocation(callback) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;
                const location = `Latitude: ${latitude}, Longitude: ${longitude}`;
                callback(location);
            },
            function(error) {
                console.log('Chyba při získávání geolokace:', error);
                callback('Neznámá');
            }
        );
    } else {
        console.log('Geolokace není podporována ve vašem prohlížeči.');
        callback('Neznámá');
    }
}
