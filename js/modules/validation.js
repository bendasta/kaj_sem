export function validateNoteTitle(title) {
    const regex = /[0-9]/; // Regular expression to match numbers

    if (regex.test(title)) {
        return false; // Return false if title contains numbers
    }

    return true; // Return true if title is valid
}