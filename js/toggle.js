export function toggleForm() {
    const updateButtonElement = document.getElementById("update-button");
    const form = document.getElementById('note-form');
    updateButtonElement.style.display = "none";
    form.classList.toggle('expanded');
}
