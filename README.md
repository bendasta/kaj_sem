# KAJ_Sem

## Téma: Poznámky

## Cíl projektu

Cílem projektu bylo naprogramovat single page aplikaci, ve které by bylo možné se jako uživatel přihlásit, zobrazit si svoje poznámky či přidat/upravit poznámku.


## Funkcionality

Uživatl se může přihlásit, když si zobrazí své poznámky a již jsou zde nějáké uloženy v local storage zobrazí se mu, pokud ne pouze se vypíše text kde je napsáno "Prozatím zde nejsou žádné poznámky".

Jako přihlášený uživatel může přidat svou poznámku u které se mu následně zobrazí kdy a kde ji přidal a obsah jako takový. Zde je validace, která kontroluje to, jestli nejsou políčka prázdná a jestli v nadpise není číslice, která tam nemají co dělat.

Také zde uživatel může libovolnou poznámku smazat nebo ji upravit. Při úpravě se mu otevře formulář pro přidání poznámky, kde je již napsán nadpis i obsah a oboje lze upravit. Nadpis stejně jako u vytváření podléhá validaci.

K tomu aby bylo možné přidat poznámku je nutné dát buď ano nebo ne ve vyskakovacím okně, kde se vás ptají na povolit polohu aby bylo možné určit geolokaci. Pokud dáte že ne pouze se vypíše že je geolokace naznámá.

Jako poslední je zde ke každému úkonu speciální zvuk.
